package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = "/logout", loadOnStartup = 1)
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String customerEmail = null;
		response.setContentType("text/html");

		try (PrintWriter pw = response.getWriter()) {
			Cookie[] cookies = request.getCookies();
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("customer_details")) {
					customerEmail = cookie.getValue();
					cookie.setMaxAge(0);
					response.addCookie(cookie);
				}
			}
			pw.print("Logout Successfully. Thank you " + customerEmail);
			pw.print("<a href='login.html'>Login</a>");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
