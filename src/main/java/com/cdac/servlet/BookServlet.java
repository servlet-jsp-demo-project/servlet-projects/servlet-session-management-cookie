package com.cdac.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/books")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public BookServlet() {
		System.out.println("Parameterless Constructor of " + this.getClass().getName());
	}

	@Override
	public void init() throws ServletException {
		System.out.println("Init Method : " + this.getClass().getName());
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String customerEmail = null;
		response.setContentType("text/html");

		try (PrintWriter pw = response.getWriter()) {
			/*
			 * Get cookies from request headers
			 */
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals("customer_details"))
						customerEmail = cookie.getValue();
				}
			} else {
				pw.print("No Cookies, Session Tracking Failed!");
			}
			/*
			 * Check cooikes for null
			 */
			pw.println("Welcome " + customerEmail + "<br>");
			pw.print("List of Books will be Displayed Here <br>");
			pw.print("<a href='logout'>Logout</a>");
		} catch (Exception e) {
			throw new ServletException("Error in doGet() method of " + this.getClass().getName());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
